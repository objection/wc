/*
 * I've presumptiously called this WC. It's not, really. WC can
 * tell the difference between bytes and chars. This is slapdash
 */

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>
#include <stdio.h>

struct wc {
	size_t lines;
	size_t chars;
	size_t words;
	bool failed;
};

struct wc wc_from_fs (FILE **fs);
struct wc wc_from_paths (char **paths);
struct wc wc_str (char *strs);
