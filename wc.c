#define _GNU_SOURCE

#include "wc.h"

#define $add_wcs(_result, _this) \
	do { \
		(_result).chars += (_this).chars; \
		(_result).lines += (_this).lines; \
		(_result).words += (_this).words; \
	} while (0)

struct wc wc_from_f (FILE *f) {

	struct wc r = {.failed = 1};

	int ch;
	while ((ch = fgetc (f)) != EOF) {
		switch (ch) {
			case '\t':
				[[fallthrough]];
			case ' ':
				break;
			case '\n':
				r.lines++;
				break;
			default:
				r.words++;
				while ((ch = fgetc (f)) != EOF && !isspace (ch));
				if (ch != EOF)
					ungetc (ch, f);
				break;
		}
	}

	r.chars = ftell (f);
	if (!ferror (f))
		r.failed = 0;
	return r;
}

struct wc wc_from_fs (FILE **fs) {
	struct wc r = {.failed = 1};
	for (FILE **_ = fs; *_; _++) {
		struct wc this = wc_from_f (*_);
		$add_wcs (r, this);
		if (fclose (*_))
			goto out;
	}
	r.failed = 0;
out:
	return r;
}

struct wc wc_from_paths (char **paths) {
	struct wc r = {.failed = 1};
	for (char **_ = paths; *_; _++) {
		FILE *f = fopen (*_, "r");
		if (!f)
			goto out;
		struct wc this = wc_from_f (f);
		$add_wcs (r, this);
		if (fclose (f))
			goto out;
	}
r.failed = 0;
out:
	return r;
}

struct wc wc_str (char *str) {

	FILE *f = fmemopen (str, (strlen (str) + 1) * sizeof *str, "r");
	if (!f) return (struct wc) {.failed = 1};

	struct wc r = wc_from_f (f);
	assert (!fclose (f));
	return r;
}


